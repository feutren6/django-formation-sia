from django.http import HttpResponse
from django.shortcuts import render

from app_pokedex.models import Pokemon

def main(request):
    pokemons = Pokemon.objects.all()
    return render(request,'main.html',{'pokemons': pokemons})
