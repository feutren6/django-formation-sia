from django.db import models

class Pokemon(models.Model):
    name = models.CharField(max_length=100)
    number = models.IntegerField()
    photo = models.ImageField(upload_to='photos', null=True, blank=True)

    class Type(models.TextChoices):
        FEU = "FEU",
        EAU = "EAU",
        PLANTE = "PLANTE",
        ELECTRIQUE = "ELECTRIQUE",
        NORMAL = "NORMAL",
        VOL = "VOL",
        COMBAT = "COMBAT",
        POISON = "POISON",
        SOL = "SOL",
        ROCHE = "ROCHE",
    
    type = models.fields.CharField(choices=Type.choices, max_length=50)

    def __str__(self):
       return f'{self.name}'