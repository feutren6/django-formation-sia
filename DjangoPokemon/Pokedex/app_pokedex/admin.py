from django.contrib import admin

from app_pokedex.models import Pokemon

admin.site.register(Pokemon)
